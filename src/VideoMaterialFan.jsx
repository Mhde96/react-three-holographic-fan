import { useVideoTexture } from "@react-three/drei";
import * as THREE from "three";
import { motion } from "framer-motion-3d";
export const VideoMaterialFan = ({ isRun }) => {
  const texture = useVideoTexture("Trophy_1_VP8.webm");
  return (
    <motion.meshStandardMaterial
      map={texture}
      initial={{ opacity: 0 }}
      animate={{
        opacity: isRun ? 1 : 0,
        transition: {
          duration: isRun ? 5 : 0.5,
          ease: "linear",
          delay: isRun ? 4 : 0,
        },
      }}
      side={THREE.DoubleSide}
    />
  );
};
