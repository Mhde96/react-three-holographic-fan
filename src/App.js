import { Backdrop, ContactShadows, Environment, OrbitControls } from "@react-three/drei";
import { Canvas } from "@react-three/fiber";
import { useEffect, useRef } from "react";
import { FanModel } from "./FanModel";

function App() {
  return (
    <Canvas shadows style={{ width: '100vw', height: '100vh' }}>

      <Scene />
    </Canvas>
  );
}

const Scene = () => {
  const orbitRef = useRef();

  useEffect(() => {
    if (orbitRef.current) {
      orbitRef.current.object.position.set(0, 0.66, 1.1)
      orbitRef.current.target.set(0, 0.5, 0)
    }
  }, [orbitRef.current])

  return <group>
    <ambientLight intensity={0.2} />
    <directionalLight position={[-10, 2, -5]} intensity={10} color="red" />
    <directionalLight position={[-1, 0, -5]} intensity={0.2} color="#0c8cbf" />

    <Environment preset='city' />
    <ContactShadows position={[0, 0, 0]} scale={5} blur={1.5} far={1} />

    <Backdrop castShadow floor={2} position={[0, -0.02, -3]} scale={[50, 10, 4]}  >
      <meshStandardMaterial color="#353540" envMapIntensity={0.1} />
    </Backdrop>


    <OrbitControls makeDefault ref={orbitRef} />

    <FanModel />
  </group>
}

export default App;
